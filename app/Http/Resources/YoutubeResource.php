<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class YoutubeResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [    
                    'published_at' => $this->snippet->publishedAt,
                    'title' => $this->snippet->title,
                    // Dependiendo el resultado, puede ser un video o una playlist
                    $this->mergeWhen(!is_null($this->id->videoId),[
                        'id' => $this->id->videoId,]),
                    $this->mergeWhen(!is_null($this->id->playlistId),[
                        'id' => $this->id->playlistId,]),
                    'description' => $this->snippet->description,
                    'thumbnail' => $this->snippet->thumbnails->medium->url,
                    'extra' => [ 'channelId' => $this->snippet->channelId,
                                 'channelTitle' => $this->snippet->channelTitle,
                                 'channelDirectLink' => 'https://www.youtube.com/channel/'.$this->snippet->channelId,
                                 'kind' => $this->id->kind,
                                 // Dependiendo el resultado, defino el link directo, ya sea video o playlist
                                 $this->mergeWhen(!is_null($this->id->videoId),['videoDirectLink' => 'https://www.youtube.com/watch?v='.$this->id->videoId,]),
                                 $this->mergeWhen(!is_null($this->id->playlistId),['playlistDirectLink' => 'https://www.youtube.com/playlist?list='.$this->id->playlistId,])
                               ]
                ];
    }
}

/* {

    "published_at": "2009-10-09T13:15:12.000Z",

    "id": "X8f5RgwY8CI",

    "title": "MUSE - Algorithm [Official Music Video]",

    "description": "Description here...",

    "thumbnail": "https://i.ytimg.com/vi/TPE9uSFFxrI/default.jpg",

    "extra": {

        "something": “extra”

    }

},

{}.... */