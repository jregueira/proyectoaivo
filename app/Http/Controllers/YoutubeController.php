<?php

namespace App\Http\Controllers;

use Google_Client;
use Google_Service_YouTube;
use Google_Service_Exception;

use Illuminate\Http\Request;
use App\Http\Resources\YoutubeResource;
use Illuminate\Support\Facades\Input;

class YoutubeController extends Controller
{
	  public function getVideos($param = null)
	  {
		  // Defino la clave de desarrollador otorgada por Google
		  $DEVELOPER_KEY = 'AIzaSyA0H0QyLMS5OCQg9y4YQlBYkqIbc1BsC-I';
		  
		  $client = new Google_Client();
		  $client->setDeveloperKey($DEVELOPER_KEY);
		  
		  try{
		  // Defino un objeto para realizar requests de la API
		  $youtube = new Google_Service_YouTube($client);
		  $param = Input::get('search');

		  // Chequeo si el parámetro de búsqueda existe
		  if(is_null($param)){
		  	return response()->json(['message' => 'The search could not be performed',
		  							 'reason' => 'A search parameter is needed',], 400);
		  }
		   
		  // Obtengo una lista de datos dependiendo el parámetro de búsqueda
		  $searchResponse = $youtube->search->listSearch('id,snippet', array(
	      'q' => $param,
	      'maxResults' => '10',));

		  // Defino una colección
		  $listOfResults = collect([]);
		  // Acumulo en la colección por cada video o playlist que encuentra
		  foreach ($searchResponse as $search) {
		  	$listOfResults->push($search);
		  }

		  // Verifico que la lista no este vacía
		  if ($listOfResults->isNotEmpty()) {
		  		YoutubeResource::withoutWrapping(); // Remove data key
		  		return YoutubeResource::collection($listOfResults);
		  } else {
		  	return response()->json(['message' => 'No videos related to your search have been found, try another parameter',], 200);
		  }

		  // Devuelvo un json si se produce la excepción
		  } catch (Google_Service_Exception $e){
			$json = json_decode($e->getMessage());
			return response()->json(['message' => $json->error->message,
									 'reason' => $json->error->errors[0]->reason,
									 'code' => $json->error->code,]);
		}
	}
}
