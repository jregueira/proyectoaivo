# Running proyect:

## Install PHP 7.0:

```
sudo apt-get install python-software-properties -y && sudo add-apt-repository ppa:ondrej/php-7.0
sudo apt-get update
sudo apt-get install php7.0
```

## Install Laravel:

```
composer global require "laravel/installer"
```

## Download proyect:

```
git clone https://jregueira@bitbucket.org/jregueira/proyectoaivo.git
```

## Install composer:

```
/path/of/the/proyect composer install
```

## Run proyect:

```
/path/of/the/proyect php artisan serve
```

Laravel Version 5.5.45

## Requirements:
* PHP >= 7.0.0
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
